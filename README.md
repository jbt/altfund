This project may be too rough to call it a proof-of-concept. It's more like an evidence-of-concept.

If you don't have the env setup, you can see what the usecase script looks like here:
https://libre.video/videos/watch/a730f750-a7b1-4a5e-8670-0117d37e18d2

The idea is that there's a smart contract which maintains the bookkeeping for an alt fund, specifically:
* Allowing the fund to approve participants and the max amount they may commit
* Capital commitments made by those participants
* Capital calls made by the fund
* Contributions made by the participants
* Distributions made by the fund

The transactions are all handled in any one token chosen when doing the initial setup for the fund.
In the user story below EOSDT was chosen due to its similarities with USD and the popularity of the latter in tradfin.

## PM/TODO some of the loose ends:
* Create UIs, obvs
* Enforcing the total-commitment cap on approve is wrong. It should be on commit. If the fund only wants to approve that amount, they may do so. But if they want to collect the whole commitment, and still allow for trading, they cap on approval needs to be higher than the total.
* The notification period should be a parameter to create, not a hard-coded constant.
* It's unrealistic to expect 3rd-party dexs/order books to learn how to play nicely with this, as they're expecting transfer to be an action on eosio.token, like a standard token. And we can't do that, due to the requirement to pre-approve. Probably the simplest approach is to implement our own super-simplistic order book, so that people can buy in or sell their commitment atomically with the payment.
* Fix bugs
    - Rounding in distribution: currently the remainder gets stuck in the contract's account, which is very wrong. Either error out to force the fund to only distribute something that's evenly divisible, or send the remainder back to the fund.
    - Almost certainly a bug or two I forget/didn't notice
* Implement notification, both on-chain (in a way the UI can discover) and using an oracle. Important for capital calls in particular.
* A security audit.

<pre>
./test/user_story.sh
I'm using some creative/convenient reinterpretations of the original use case to "leverage EOS" to do some of these things for me.
In some cases there's not an obvious replacement in context.
In other cases, it just takes a bit of imagination to see what the gap would be filled with.

Limitations!
Some of the parts of this that absolutely would be necessary if it were a real project, for now I'm just pretending are present:

- CRM. Just having an eos account name isn't good enough - if this were a real, production system you'd need to
be able to tie that back to contact info, PII, legal documents, etc. For now I'm just faking it.
- The public EOS blockchain. I'm actually running this on my own local, disconnected node - a sandbox.
- A real UI. This is a big difference I wish I had more time to work on that part.
I'll have my pretend users taking actions by way of cleos (Command Line EOS)
But I'll try to make note where that action might have taken place in a real, production system
- There are bugs. At least one I know about that isn't worth fixing for a POC, but there's more I'm sure.
- Likewise, something like this needs a careful infosec audit before going public.

With those caveats out of the way, let's get to my creative interpretation of the user story Ruchir gave me.


   ###   Original Description   ### 
     (1)(a) create a... legal entity, call it PE Corp. create a bank account #PE987654321 owned by PE Corp.\n

   ###   My interpretation:   ###
    In my approach any legal entity will be represented by an EOS account.
    If there's an ownership relationship, whoever has the private keys to that account is the owner.
    - and in this script my wallet has all the keys, so I'm playing all the roles of a one-man show, but one can imagine.
    Bank account(s) and/or cash account(s) are just the token/coin balances that account happens to have on the EOS blockchain itself.
    Also, for reproducibility's sake (every account name is unique) account names will be suffixed by a random word.
    Hope that's not too distracting.
    
    # Where we might imagine this happening if this were real: probably GUI wallet app PE Corp has their infosec folks tending to #

Creating account for PE Corp as pe.corp.nast stored in /tmp/tmp.mViCwAeEcw.pe.corp
imported private key for: EOS8RAKngR84QhWK1zL6ZYXw4wrf5XQfakWuR5yfSLqJv8W5PFHRb
imported private key for: EOS7eqYcQStaTpqhAn4GCa5g9juAPKmzfXLGzR76gBLfZtmEh3obc
#         eosio <= eosio::newaccount            "0000000000ea305590b199a05e8a80aa010000000100036c170ed7fa8a1fd8aa6f33a75b8639cab62a93a269a5e36678786...
    Account created for PE Corp : pe.corp.nast 

   ###   Original Description   ### 
     (1)(b) create a private equity fund with 100mm total commitment.\n

   ###   My interpretation:   ###
    The smart contract runs on its own account, pefund, and is essentially an alt fund factory.
    So this step is the PE Corp account asking the smart contract to set it up as a fund.
    
    I'm not going to step through every argument to every cleos command, but this one's quite instructive:
    cleos  : talk to EOS (on the Command Line)
    push   : add something to the blockchain
    action : specifically an interaction with a contract is getting pushed to the end of the chain
    pefund : this is the contract's account
    create : this is the specific API endpoint being hit. In this case we're creating a new fund.
    ['pe.corp.nast', '100000000 EOSDT'] : these are the parameters to the creation, i.e. create(pe.corp.nast, 100000000 EOSDT)
    That first argument is saying that pe.corp.nast will be the EOS account associated with this fund - the fund's account
    The second argument is the total commitment but not just that...
    10 million
    EOSDT - the name of a token. Note the lack of any decimal places. If I had written 100000000.00 it would be resolved down to the cents.
    -p "pe.corp.nast@active"  :  sign it using that account's active key
    
    That total commitment argument is relevant because it ties this fund to using EOSDT with no decimal places for ALL of its transactions.
    Commitments will be made in terms of whole EOSDT. So will contributions. And disbursments.
    If this is a problem we could probably loosen this requirement.
    
    Why did I choose EOSDT? Because the original problem was phrased in terms of USD. And EOSDT's exchange rate with USD is
    reasonably stable, relatively to other cryptos, due to its market pegging mechanism. Granted, that peg is becoming looser these days (thanks China)...
    But it's still a realistic example of the kind of asset these sorts of players are likely to use on a blockchain,
    since most of their business is going to be USD-based and they might not want to calculate exchange risk, and USD is
    just more familiar... and EOSDT are kind of USD-ish (1 EOSDT is usually worth 90-something cents)
    
    # Where we might imagine this happening if this were real: probably a web app GUI that is specific to this contract, using ESR to connect to the wallet and one of the JS RPC libraries. If we go full-on it could be hosted on IPFS, but it doesn't need to be. #

#        pefund <= pefund::create               {"fund_account":"pe.corp.nast","total_commitment":"100000000 EOSDT"}

   ###   Original Description   ### 
     (2)(a) create a ‘tradeable security’ PE_ABC\n

   ###   My interpretation:   ###
    
    Right now I'm going with the lame-o version of the idea that you could 'trade' this account by handing over the private keys.
    
    I did seriously consider making a separate contract that just issues a bunch of tokens (representing shares of ownership) once
    and then has a read-only query to report back its owners and their proportions. Then the original contract could attempt to
    detect the specialness of the account.
    
    But if you read ahead, that kind of complexity isn't really needed in this case.
    If it's desired and there's still time left in HackWeek let me know.
    
    # Where we might imagine this happening if this were real: Any wallet of their choosing, really. But if bundling up ETFs and SPVs and the like as EOS accounts became normal, there'd probably be some cookie-cutter service for it to cross the legal is and dot the ts. #
    

Creating account for PE_ABC as pe.abc.snooz stored in /tmp/tmp.mViCwAeEcw.pe.abc
imported private key for: EOS8Zh8NX3bMTqj1WcfjARoe35RqEUAM9mNdnkQ5F9Jv5acgfR8s4
imported private key for: EOS6DfHmxtYJNF3iVRNBnhQhSXtSiKjQ4nkNKsmduprqFWheo4QK5
#         eosio <= eosio::newaccount            "0000000000ea3055f0299d18a06380aa01000000010002af3a17349eac3888c2786e89c8aee8019fffdac516d623c5b61dd...
    Account created for PE_ABC : pe.abc.snooz 

   ###   Original Description   ### 
     (2)(b) ... PE_ABC that buys a commitment of 5mm from PE Corp.\n

   ###   My interpretation:   ###
    
    Woah, now hold on there. Right now PE_ABC is in no way known to PE Corp as far as the system knows.
    It could easily be a faceless, anonymous child in Romania promising to pay you 5mm. And what should you do with such an offer?
    
    Ignore it.
    
    That's why this contract maintains a list of specially-permissioned accounts who are approved to make a commitment and of how much.
    So, first, as PE Corp. let's acknowledge that we do trust this PE_ABC vehicle and add it to the list.
    
    This command below says that PE_ABC gains permission to commit *up to* 50mm.
    
    Right now there's a restriction that the sum of your approved levels can't go above your fund's total commitment. Is that wrong?
    
    # Where we might imagine this happening if this were real: This would be a *DIFFERENT* app (these are probably web apps, but don't have to be) from the one I mentioned before, because this is internal-facing. That is, this interface is for the fund managers, not institutions looking to buy in. #

#        pefund <= pefund::approve              {"fund":"pe.corp.nast","investor":"pe.abc.snooz","max_commitment":"50000000 EOSDT"}

   ###   Original Description   ### 
     (2)(c) ... buys a commitment of 5mm ... \n

   ###   My interpretation:   ###
    
    OK now we can do it for real. Notice this time we're signing the request as PE_ABC, not PE Corp. Because we're wearing the other hat now.
    Note that I still have zero decimal places and it's still in EOSDT. The fund demands it - if I chose a different token this would error out.
    

#        pefund <= pefund::commit               {"investor":"pe.abc.snooz","fund":"pe.corp.nast","add":"5000000 EOSDT"}

   ###   Original Description   ### 
     (3) create a legal entity - ABC Inc. And, create a bank account #123456789 that is owned by ABC Inc.\n

   ###   My interpretation:   ###
    
    Going with this approach to 'bank accounts', this is also super simple.
    

Creating account for ABC Inc as abc.inc.magn stored in /tmp/tmp.mViCwAeEcw.abc.inc
imported private key for: EOS78bsaVBAo78H16ATAG21uNSeGEtzotVNX2DG8spRWXFX162L5P
imported private key for: EOS68k5Fnvi31bmDdA8uUYspAwTCrawY7gNWR7tNmUdjiCBS53jd2
#         eosio <= eosio::newaccount            "0000000000ea3055309991004d07d03101000000010002a40fa413f0fa37ff99ba60da1ec47f5a6d4082ad00c6308f5975e...
    Account created for ABC Inc : abc.inc.magn 

   ###   Original Description   ### 
     (4) ABC Inc. buys PE_ABC with a commitment of 5 mm\n

   ###   My interpretation:   ###
    
    OK, so in this sim this just happens in our minds.
    
    # Where we might imagine this happening if this were real: #
    Really boils down to how one imagines chained ownership being modeled/handled on the blockchain.
    I'm guessing it's a different smart contract with a different web app.
    But in this case we don't want to allow just anyone to buy it. So I'm not sure we really _want_ to tokenize _unfunded_ commitments.
    Funded commitments - now that could be something.


   ###   Original Description   ### 
     (5) PE Corp. issues a ‘capital call’ for 10%, total 10mm for investment into certain vehicles. And, creates a ‘receivable’ of 500k against PE_ABC\n

   ###   My interpretation:   ###
    
    Now I haven't actually made a bunch of other accounts and sold the other 95mm, so they're not really going to get 10mm from this 10%. They _are_, however, going to be asking for the 500k.
    
    I'm thinking we go outside the blockchain to do the legally-required notification of their like 10 business days or whatnot.
    So the contract isn't going to take any external action here. It's just going to mark some folks as not being in good standing anymore.

#        pefund <= pefund::capitalcall          {"fund":"pe.corp.nast","percent":10}

   ###   Original Description   ### 
     5.b. ... \n

   ###   My interpretation:   ###
    It wasn't mentioned explicitly in the original case, but when you issue a capital call
    you need to notify all those people that they need to pay you and they're on the clock.
    So, let's see who's fallen behind, by which I mean
    the % of their commitment that is funded is less than the sum of the all the capital calls to this point.
    Those folks owe the fund!
    
    By the way in the output below if your due date is the epoch aka time zero aka 1970-01-01T00:00:00.000 ...
    that really means you're caught up and no more contributions are currently due.
    
    # Where we might imagine this happening if this were real: In connection with a CRM so you can actually send these people emails or whatever. #

{
  "rows": [{
      "fund": "pe.corp.nast",
      "investor": "pe.abc.snooz",
      "commitment": "5000000 EOSDT",
      "contribution": "0 EOSDT",
      "approval": "50000000 EOSDT",
      "distributed": "0 EOSDT",
      "due": "2021-11-19T14:39:09.000"
    }
  ],
  "more": false,
  "next_key": ""
}
"pe.abc.snooz needs to top up by 2021-11-19T14:39:09.000"

   ###   Original Description   ### 
     (6) PE_ABC has a ‘payable’ booked for 500k\n

   ###   My interpretation:   ###
    
    Yep, they can see it in that output above, and we notified them of it.
    We certainly could make some sort of web dashboard to show them what they owe and when. Notice that I didn't even have to sign that command - it's public info.
    I'm sure some accountants would be furious with me for not taking the dual entries with sufficient gravitas.
    

  ##  Current cash balances  ##
      pe.corp.nast : 
      pe.abc.snooz : 
      abc.inc.magn : 
      pefund :  4321341 EOSDT

   ###   Original Description   ### 
     (7) ABC Inc. ‘transfers or pays’ 500k from 123456789 to PE_ABC\n

   ###   My interpretation:   ###
    
    This should be straightforward but amusingly enough up until this point in the simulation no one has actually had any money at all.
    But this is our sandbox not a real blockchain, we can do what we want.
    So let's give ABC a windfall and they can turn around and pay PE_ABC.
    

#   eosio.token <= eosio.token::issue           {"to":"eosio","quantity":"1234567890 EOSDT","memo":"memo"}
#   eosio.token <= eosio.token::transfer        {"from":"eosio","to":"abc.inc.magn","quantity":"123456789 EOSDT","memo":"Once the issuer always the ...
#         eosio <= eosio.token::transfer        {"from":"eosio","to":"abc.inc.magn","quantity":"123456789 EOSDT","memo":"Once the issuer always the ...
#  abc.inc.magn <= eosio.token::transfer        {"from":"eosio","to":"abc.inc.magn","quantity":"123456789 EOSDT","memo":"Once the issuer always the ...
  ##  Current cash balances  ##
      pe.corp.nast : 
      pe.abc.snooz : 
      abc.inc.magn :  123456789 EOSDT
      pefund :  4321341 EOSDT
#   eosio.token <= eosio.token::transfer        {"from":"abc.inc.magn","to":"pe.abc.snooz","quantity":"500000 EOSDT","memo":"Memo: to cover the 10% ...
#  abc.inc.magn <= eosio.token::transfer        {"from":"abc.inc.magn","to":"pe.abc.snooz","quantity":"500000 EOSDT","memo":"Memo: to cover the 10% ...
#  pe.abc.snooz <= eosio.token::transfer        {"from":"abc.inc.magn","to":"pe.abc.snooz","quantity":"500000 EOSDT","memo":"Memo: to cover the 10% ...
  ##  Current cash balances  ##
      pe.corp.nast : 
      pe.abc.snooz :  500000 EOSDT
      abc.inc.magn :  122956789 EOSDT
      pefund :  4321341 EOSDT

   ###   Original Description   ### 
     (8) PE_ABC ‘transfers or pays’ 500k to PE987654321\n

   ###   My interpretation:   ###
    
    The smart contract triggers some code that executes whenever anyone sends it money. So this should just work.
    

  ##  Current cash balances  ##
      pe.corp.nast : 
      pe.abc.snooz :  500000 EOSDT
      abc.inc.magn :  122956789 EOSDT
      pefund :  4321341 EOSDT
#   eosio.token <= eosio.token::transfer        {"from":"pe.abc.snooz","to":"pefund","quantity":"500000 EOSDT","memo":"pe.corp.nast"}
#  pe.abc.snooz <= eosio.token::transfer        {"from":"pe.abc.snooz","to":"pefund","quantity":"500000 EOSDT","memo":"pe.corp.nast"}
#        pefund <= eosio.token::transfer        {"from":"pe.abc.snooz","to":"pefund","quantity":"500000 EOSDT","memo":"pe.corp.nast"}
#   eosio.token <= eosio.token::transfer        {"from":"pefund","to":"pe.corp.nast","quantity":"500000 EOSDT","memo":"Contribution from pe.abc.snoo...
#        pefund <= eosio.token::transfer        {"from":"pefund","to":"pe.corp.nast","quantity":"500000 EOSDT","memo":"Contribution from pe.abc.snoo...
#  pe.corp.nast <= eosio.token::transfer        {"from":"pefund","to":"pe.corp.nast","quantity":"500000 EOSDT","memo":"Contribution from pe.abc.snoo...
  ##  Current cash balances  ##
      pe.corp.nast :  500000 EOSDT
      pe.abc.snooz :  0 EOSDT
      abc.inc.magn :  122956789 EOSDT
      pefund :  4321341 EOSDT

   ###   Original Description   ### 
     (9) Create a simple view that displays:
Total commitment = 5mm
Total contribution = 500k
Total distribution = 0
Total unfunded commitment = 4.5mm\n

   ###   My interpretation:   ###
    
    The simplest of all views would be to simply dump that row of data from the table like I did earlier.
    Let's see what that looks like, then I'll open up a browser to show something that looks more natural to people who don't read JSON all day.
    

{
  "fund": "pe.corp.nast",
  "investor": "pe.abc.snooz",
  "commitment": "5000000 EOSDT",
  "contribution": "500000 EOSDT",
  "approval": "50000000 EOSDT",
  "distributed": "0 EOSDT",
  "due": "1970-01-01T00:00:00.000"
}
See your browser.

   ###   Original Description   ### 
     (10) A distribution is a reverse transaction flow with money going from PE Corp. all the way to the bank account 123456789\n

   ###   My interpretation:   ###
    It wasn't spelled out the actual rules for how distributions work so I made up my own.
    
    The fund owning account sends the contract money with a memo of its own account name to trigger a distribution.
    If the memo specified some other fund's name it would be treated as a contribution, BTW.
    Anyhow, I then select out only those accounts that are 'in good standing'.
    Which means that they've contributed a % of their commitment equal to or greater than the amount that's been called so far.
    If you're delinquent you're not eligible for distrubtion :)
    Then I try to raise everyone to the same distributed
    which is the amount that's been distrubted to them as a % of their commitment.
    The target for that ratio is chosen based on how much was sent for the distribution.

  ##  Current cash balances  ##
      pe.corp.nast :  500000 EOSDT
      pe.abc.snooz :  0 EOSDT
      abc.inc.magn :  122956789 EOSDT
      pefund :  4321341 EOSDT
#   eosio.token <= eosio.token::transfer        {"from":"pe.corp.nast","to":"pefund","quantity":"100000 EOSDT","memo":"pe.corp.nast"}
#  pe.corp.nast <= eosio.token::transfer        {"from":"pe.corp.nast","to":"pefund","quantity":"100000 EOSDT","memo":"pe.corp.nast"}
#        pefund <= eosio.token::transfer        {"from":"pe.corp.nast","to":"pefund","quantity":"100000 EOSDT","memo":"pe.corp.nast"}
#   eosio.token <= eosio.token::transfer        {"from":"pefund","to":"pe.abc.snooz","quantity":"100000 EOSDT","memo":"Distribution up to ratio: 0.0...
#        pefund <= eosio.token::transfer        {"from":"pefund","to":"pe.abc.snooz","quantity":"100000 EOSDT","memo":"Distribution up to ratio: 0.0...
#  pe.abc.snooz <= eosio.token::transfer        {"from":"pefund","to":"pe.abc.snooz","quantity":"100000 EOSDT","memo":"Distribution up to ratio: 0.0...
  ##  Current cash balances  ##
      pe.corp.nast :  400000 EOSDT
      pe.abc.snooz :  100000 EOSDT
      abc.inc.magn :  122956789 EOSDT
      pefund :  4321341 EOSDT
</pre>
