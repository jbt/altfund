#pragma once

#include <eosio/asset.hpp>
#include <eosio/eosio.hpp>
#include <eosio/system.hpp>

namespace jt {
    
    class [[eosio::contract("pefund")]] pefund : public eosio::contract {
      public:
          using eosio::contract::contract;

         [[eosio::action]]
         void create( 
              eosio::name  const& fund_account
            , eosio::asset const& total_commitment
            );

         [[eosio::action]]
         void approve(
               eosio::name         fund
             , eosio::name         investor
             , eosio::asset const& max_commitment
             );

         [[eosio::action]]
         void commit( 
              eosio::name  const& investor
            , eosio::name  const& fund
            , eosio::asset const& add 
            );

         [[eosio::action]]
         void capitalcall( 
              eosio::name const& fund
            , std::uint16_t percent
            );

         [[eosio::on_notify("eosio.token::transfer")]]
         void on_receive(
              eosio::name const& investor
            , eosio::name const& contract
            , eosio::asset const& quantity
            , std::string const& memo
            );
         
         [[eosio::action]]
         void transfer( 
              eosio::name  const& from
            , eosio::name  const& to
            , eosio::asset const& quantity
            , eosio::name  const& fund
            );

      private:
        struct [[eosio::table]] fund {
            //scope is the contract
            eosio::name  fund_account;
            eosio::asset total_commitment;
            std::uint16_t called_percent;

            std::uint64_t primary_key() const { return fund_account.value; }
        };
        struct [[eosio::table]] account {
            //scope is the fund;
            eosio::name  fund;
            eosio::name  investor;
            eosio::asset commitment;
            eosio::asset contribution;
            eosio::asset approval;
            eosio::asset distributed;
            eosio::time_point due;

            std::uint64_t primary_key() const {
                return investor.value;
            }
        };

         typedef eosio::multi_index<   "funds"_n,    fund> funds;
         typedef eosio::multi_index<"accounts"_n, account> accounts;
         
         eosio::asset get_total_commitment( eosio::name );
         void contribution( eosio::name investor, eosio::name, eosio::asset sent );
         void distribution( eosio::name, eosio::asset sent );
         void validate( eosio::asset const& to_check, eosio::symbol const& unit );
         void validate( eosio::asset const& to_check, eosio::asset const& compatible_with );
         bool in_good_standing( fund const& fnd, account const& acct ) const;
         void pay( eosio::name const& recip, eosio::asset const& amt, std::string memo );
   };

}
