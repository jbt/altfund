#include <pefund/pefund.hpp>
#include <numeric>

using me = jt::pefund;

namespace e = eosio;

void me::create( 
      e::name     const& creator
    , e::asset    const& created_total_commitment
    )
{
    e::require_auth( creator );
    //validate(created_total_commitment);

    funds table{ get_self(), get_self().value };
    auto existing = table.find( creator.value );
    e::check( existing == table.end(), "Fund already created." );
    
    table.emplace( creator, [&](auto&f){
            f.fund_account = creator;
            f.total_commitment = created_total_commitment;
            f.called_percent = 0;
        } );
}

void me::approve( e::name fund_name, e::name investor, e::asset const& max_commitment )
{   
    e::require_auth( fund_name );
    
    funds ftable{ get_self(), get_self().value };
    auto fund = ftable.find( fund_name.value );
    e::check( fund != ftable.end(), "Fund not found." );
    
    validate( max_commitment, fund->total_commitment );
    
    accounts table{ get_self(), fund_name.value };
    auto zero = e::asset{0L, max_commitment.symbol};
    auto prev_sum = std::accumulate( 
          table.cbegin()
        , table.cend()
        , zero
        , [](auto&t,auto&r){return t+r.approval;}
        );
    
    auto existing = table.find( investor.value );
    if ( existing == table.end() )
    {
        e::check( fund->total_commitment >= prev_sum + max_commitment, "One cannot approve commitments above and beyond the fund's total commitment." );
        table.emplace( get_first_receiver(), [&](auto&p) {
                p.fund = fund_name;
                p.investor = investor;
                p.distributed = p.commitment = p.contribution = zero;
                p.approval = max_commitment;
            });
    }
    else
    {
        e::check( fund->total_commitment >= prev_sum + max_commitment - existing->approval, "One cannot approve commitments above and beyond the fund's total commitment." );
        table.modify( existing, get_first_receiver(), [&](auto&p) {
                p.approval = max_commitment;//questionable whether this should maybe be += ... who needs those Ricardean contracts anyhow
            });
    }
}

void me::transfer( 
      e::name  const& from_name
    , e::name  const& to_name
    , e::asset const& quantity
    , e::name  const& fund
    )
{
    e::check( from_name != to_name, "cannot transfer to self" );
    e::require_auth( from_name );
    e::check( e::is_account( to_name ), "to account does not exist");
    accounts table{ get_self(), fund.value };
    auto from = table.find( from_name.value );
    e::check(from != table.end(), "You don't have any interest in that fund to transfer.");
    validate(quantity, from->approval);
    e::check( from->commitment >= quantity, "You don't have that much interest to transfer." );
    auto to = table.find( to_name.value );
    e::check( to != table.end(), to_name.to_string() + " is not an approved recipient of interest in " + fund.to_string() );
    e::check( quantity + to->commitment <= to->approval, "Recipient not approved for that much commitment." );
    table.modify( from, from_name, [&](auto&r){r.commitment -= quantity;} );
    table.modify(   to, from_name, [&](auto&r){r.commitment += quantity;} );
}

void me::commit( eosio::name const& investor, eosio::name const& fund, e::asset const& add )
{
    require_auth( investor );
    accounts table{ get_self(), fund.value };
    auto entry = table.find( investor.value );
    e::check( entry != table.end(), "You have not been approved to take out this commitment." );
    validate( add, entry->approval );
    e::check( entry->commitment + add <= entry->approval, "You haven't been approved for that large of a commitment." );
    table.modify( entry, investor, [&add](auto&r){r.commitment += add;} );
}

void me::capitalcall( e::name const& fund, std::uint16_t percent )
{
    require_auth( fund );
    e::check( percent > 0, "the % being called must be positive" );
    funds table{ get_self(), get_self().value };
    auto record = table.find( fund.value );
    e::check( record != table.end(), "Couldn't find the fund to call: " + fund.to_string() );
    e::check( percent + record->called_percent <= 100, "Can't call more than 100%." );
    table.modify( record, record->fund_account, [percent](auto&r){r.called_percent+=percent;} );
    accounts to_check{ get_self(), fund.value };
    for ( auto& account : to_check )
    {
        if ( ! account.due.sec_since_epoch() && ! in_good_standing(*record, account) )
        {
            to_check.modify( account, fund, [](auto&a){a.due = e::current_time_point() + e::days(15);} );
        }
    }
}

void me::on_receive( e::name const& from, e::name const& to, e::asset const& value_sent, std::string const& memo )
{
    if ( from == get_self() ) {//don't care about outgoing trx 
        return;
    }
    e::check( to == get_self(), "Why am I getting a notification of a send to " + to.to_string() );
    e::check( ! memo.empty(), "Your memo needs to be the name of the fund you're contributing to OR distributing for, not " + memo );
    e::check( memo.size() < 15, "Your memo needs to be the name of the fund you're contributing to OR distributing for, not " + memo );
    e::name nm{memo};
    
    funds funds_table{ get_self(), get_self().value };
    auto fund = funds_table.find( nm.value );
    e::check( fund != funds_table.end(), nm.to_string() + " not found." );
    validate( value_sent, fund->total_commitment );
    
    if ( from == fund->fund_account )
    {
        distribution( nm, value_sent );
    }
    else 
    {
        contribution( from, nm, value_sent );
    }
}

void me::contribution( e::name investor, e::name issuer, e::asset sent )
{
    accounts account_table{ get_self(), issuer.value };
    auto acct = account_table.find( investor.value );
    e::check( acct != account_table.end(), "You don't have a commitment in " + investor.to_string() );
    
    validate( sent, acct->commitment );
    
    e::check( sent + acct->contribution <= acct->commitment, "Your commitment isn't that large." );
    account_table.modify( acct, get_self(), [&](auto&r){r.contribution += sent;
        e::check(r.contribution.amount>0,r.contribution.to_string()+"+="+sent.to_string());
    } );
    funds fs{ get_self(), get_self().value };
    auto f = fs.find( issuer.value );
    acct = account_table.find( investor.value );
    if ( f != fs.end() && in_good_standing(*f, *acct) )
    {
        account_table.modify( acct, get_self(), [](auto&a){a.due = e::time_point{};} );
    }
    pay( issuer, sent, "Contribution from " + investor.to_string() );
}
void me::distribution( e::name sender, e::asset sent )
{
    funds funds_table{ get_self(), get_self().value };
    auto fund = funds_table.find( sender.value );
    e::check( fund != funds_table.end(), "Fund not found." );
    validate( sent, fund->total_commitment );
    accounts account_table{ get_self(), sender.value };
    //Would be really nice if I could use the ranges TS here. Check into that some other time
    e::asset sum_of_commitments{ 0L, sent.symbol };
    e::asset sum_of_previous_distributions{ 0L, sent.symbol };
    for ( auto& acct : account_table )
    {
        if ( in_good_standing(*fund, acct) )
        {
            sum_of_commitments += acct.commitment;
            sum_of_previous_distributions += acct.distributed;
        }
    }
    auto target_ratio = static_cast<double>(sent.amount + sum_of_previous_distributions.amount) / sum_of_commitments.amount;
    auto memo = "Distribution up to ratio: " + std::to_string(target_ratio);
    for ( auto& acct : account_table )
    {
        if ( in_good_standing(*fund, acct) )
        {
            auto target_dist = e::asset{ static_cast<std::int64_t>(target_ratio * acct.commitment.amount), sent.symbol };//Some real Superman III / Office Space stuff here
            auto payment = target_dist - acct.distributed;
            if ( payment.amount > 0 )
            {
                pay( acct.investor, target_dist - acct.distributed, memo );
                account_table.modify( acct, get_self(), [&](auto&r){r.distributed = target_dist;} );
            }
        }
    }
}

void me::validate( e::asset const& a, e::asset const& compatible_with )
{
    validate( a, compatible_with.symbol );
}
void me::validate( e::asset const& a, e::symbol const& unit )
{
    e::check( a.is_valid(), "Amounts of money need to be valid?" );
    e::check( a.amount > 0, "Amount transferred must be positive." );
    e::check( a.symbol == unit, "This fund only accepts commitments and contributions in " + unit.code().to_string() );
}

bool me::in_good_standing( fund const& fnd, account const& acct ) const
{
    e::check( acct.contribution.symbol == acct.commitment.symbol, "Inconsistent monetary units, internally." );
    auto pct_funded = acct.contribution.amount * 100 / acct.commitment.amount;
    return pct_funded >= fnd.called_percent;
}

void me::pay( eosio::name const& recip, eosio::asset const& amt, std::string memo )
{
    e::check(recip != get_self(), "Not allowed to pay myself in some sort of weird recursive way." );
    //TODO
    e::action{
          e::permission_level{get_self(), "active"_n}
        , "eosio.token"_n
        , "transfer"_n
        , std::make_tuple(get_self(), recip, amt, memo)
        }.send();
}
