#!/usr/bin/bash -e
source `dirname "${0}"`/bash.env
background_deploy
scroll <<PROLOGUE

I'm using some creative/convenient reinterpretations of the original use case to "leverage EOS" to do some of these things for me.
In some cases there's not an obvious replacement in context. 
In other cases, it just takes a bit of imagination to see what the gap would be filled with.

Limitations!
Some of the parts of this that absolutely would be necessary if it were a real project, for now I'm just pretending are present:

 - CRM. Just having an eos account name isn't good enough - if this were a real, production system you'd need to 
      be able to tie that back to contact info, PII, legal documents, etc. For now I'm just faking it.
 - The public EOS blockchain. I'm actually running this on my own local, disconnected node - a sandbox.
 - A real UI. This is a big difference I wish I had more time to work on that part.
      I'll have my pretend users taking actions by way of cleos (Command Line EOS)
      But I'll try to make note where that action might have taken place in a real, production system
 - There are bugs. At least one I know about that isn't worth fixing for a POC, but there's more I'm sure.
 - Likewise, something like this needs a careful infosec audit before going public.
      
With those caveats out of the way, let's get to my creative interpretation of the user story Ruchir gave me.

PROLOGUE

Step '(1)(a) create a... legal entity, call it PE Corp. create a bank account #PE987654321 owned by PE Corp.' <<MY
    In my approach any legal entity will be represented by an EOS account. 
    If there's an ownership relationship, whoever has the private keys to that account is the owner.
        - and in this script my wallet has all the keys, so I'm playing all the roles of a one-man show, but one can imagine.
    Bank account(s) and/or cash account(s) are just the token/coin balances that account happens to have on the EOS blockchain itself.
    Also, for reproducibility's sake (every account name is unique) account names will be suffixed by a random word. 
    Hope that's not too distracting.
    
    # Where we might imagine this happening if this were real: probably GUI wallet app PE Corp has their infosec folks tending to #
MY

create_account 'PE Corp'
pe=`account 'PE Corp'`

Step '(1)(b) create a private equity fund with 100mm total commitment.' <<MY
    The smart contract runs on its own account, pefund, and is essentially an alt fund factory.
    So this step is the PE Corp account asking the smart contract to set it up as a fund.

    I'm not going to step through every argument to every cleos command, but this one's quite instructive:
        cleos  : talk to EOS (on the Command Line)
        push   : add something to the blockchain
        action : specifically an interaction with a contract is getting pushed to the end of the chain
        pefund : this is the contract's account
        create : this is the specific API endpoint being hit. In this case we're creating a new fund.
        ['${pe}', '100000000 EOSDT'] : these are the parameters to the creation, i.e. create(${pe}, 100000000 EOSDT)
            That first argument is saying that ${pe} will be the EOS account associated with this fund - the fund's account
            The second argument is the total commitment but not just that...
                10 million 
                EOSDT - the name of a token. Note the lack of any decimal places. If I had written 100000000.00 it would be resolved down to the cents.
        -p "${pe}@active"  :  sign it using that account's active key
    
    That total commitment argument is relevant because it ties this fund to using EOSDT with no decimal places for ALL of its transactions.
    Commitments will be made in terms of whole EOSDT. So will contributions. And disbursments. 
    If this is a problem we could probably loosen this requirement.
    
    Why did I choose EOSDT? Because the original problem was phrased in terms of USD. And EOSDT's exchange rate with USD is
        reasonably stable, relatively to other cryptos, due to its market pegging mechanism. Granted, that peg is becoming looser these days (thanks China)...
        But it's still a realistic example of the kind of asset these sorts of players are likely to use on a blockchain,
        since most of their business is going to be USD-based and they might not want to calculate exchange risk, and USD is
        just more familiar... and EOSDT are kind of USD-ish (1 EOSDT is usually worth 90-something cents)

    # Where we might imagine this happening if this were real: probably a web app GUI that is specific to this contract, using ESR to connect to the wallet and one of the JS RPC libraries. If we go full-on it could be hosted on IPFS, but it doesn't need to be. #
MY

contract_action create "[\"${pe}\", \"100000000 EOSDT\"]" -p "${pe}@active"

Step '(2)(a) create a ‘tradeable security’ PE_ABC' <<MY

Right now I'm going with the lame-o version of the idea that you could 'trade' this account by handing over the private keys.

I did seriously consider making a separate contract that just issues a bunch of tokens (representing shares of ownership) once 
    and then has a read-only query to report back its owners and their proportions. Then the original contract could attempt to 
    detect the specialness of the account.

But if you read ahead, that kind of complexity isn't really needed in this case. 
If it's desired and there's still time left in HackWeek let me know.

# Where we might imagine this happening if this were real: Any wallet of their choosing, really. But if bundling up ETFs and SPVs and the like as EOS accounts became normal, there'd probably be some cookie-cutter service for it to cross the legal is and dot the ts. #

MY

create_account 'PE_ABC'
pe_abc=`account 'PE_ABC'`

Step '(2)(b) ... PE_ABC that buys a commitment of 5mm from PE Corp.' <<MY

Woah, now hold on there. Right now PE_ABC is in no way known to PE Corp as far as the system knows.
It could easily be a faceless, anonymous child in Romania promising to pay you 5mm. And what should you do with such an offer?

Ignore it.

That's why this contract maintains a list of specially-permissioned accounts who are approved to make a commitment and of how much.
So, first, as PE Corp. let's acknowledge that we do trust this PE_ABC vehicle and add it to the list.

This command below says that PE_ABC gains permission to commit *up to* 50mm. 

Right now there's a restriction that the sum of your approved levels can't go above your fund's total commitment. Is that wrong?

# Where we might imagine this happening if this were real: This would be a *DIFFERENT* app (these are probably web apps, but don't have to be) from the one I mentioned before, because this is internal-facing. That is, this interface is for the fund managers, not institutions looking to buy in. #
MY

contract_action approve "[\"${pe}\", \"${pe_abc}\", \"50000000 EOSDT\"]" -p "${pe}@active"

Step '(2)(c) ... buys a commitment of 5mm ... ' <<MY

OK now we can do it for real. Notice this time we're signing the request as PE_ABC, not PE Corp. Because we're wearing the other hat now.
Note that I still have zero decimal places and it's still in EOSDT. The fund demands it - if I chose a different token this would error out.

MY

contract_action commit "[\"${pe_abc}\", \"${pe}\", \"5000000 EOSDT\"]" -p "${pe_abc}@active"

Step '(3) create a legal entity - ABC Inc. And, create a bank account #123456789 that is owned by ABC Inc.' <<MY

Going with this approach to 'bank accounts', this is also super simple.

MY

create_account 'ABC Inc'
abc=`account 'ABC Inc'`

Step '(4) ABC Inc. buys PE_ABC with a commitment of 5 mm' <<MY

OK, so in this sim this just happens in our minds. 

# Where we might imagine this happening if this were real: #
Really boils down to how one imagines chained ownership being modeled/handled on the blockchain. 
I'm guessing it's a different smart contract with a different web app. 
But in this case we don't want to allow just anyone to buy it. So I'm not sure we really _want_ to tokenize _unfunded_ commitments.
Funded commitments - now that could be something.
MY


Step '(5) PE Corp. issues a ‘capital call’ for 10%, total 10mm for investment into certain vehicles. And, creates a ‘receivable’ of 500k against PE_ABC' <<MY

Now I haven't actually made a bunch of other accounts and sold the other 95mm, so they're not really going to get 10mm from this 10%. They _are_, however, going to be asking for the 500k.

I'm thinking we go outside the blockchain to do the legally-required notification of their like 10 business days or whatnot. 
So the contract isn't going to take any external action here. It's just going to mark some folks as not being in good standing anymore.
MY

contract_action capitalcall "[\"${pe}\", 10]" -p "${pe}@active"

Step "5.b. ... " <<MY
    It wasn't mentioned explicitly in the original case, but when you issue a capital call 
    you need to notify all those people that they need to pay you and they're on the clock. 
    So, let's see who's fallen behind, by which I mean 
    the % of their commitment that is funded is less than the sum of the all the capital calls to this point.
    Those folks owe the fund!
    
    By the way in the output below if your due date is the epoch aka time zero aka 1970-01-01T00:00:00.000 ...
    that really means you're caught up and no more contributions are currently due.
    
    # Where we might imagine this happening if this were real: In connection with a CRM so you can actually send these people emails or whatever. #
MY

eos get table pefund "${pe}" accounts
eos get table pefund "${pe}" accounts | jq '.rows[] | select(.due > "1971") | (.investor + " needs to top up by " + .due)'

Step '(6) PE_ABC has a ‘payable’ booked for 500k' <<ME

Yep, they can see it in that output above, and we notified them of it. 
We certainly could make some sort of web dashboard to show them what they owe and when. Notice that I didn't even have to sign that command - it's public info.
I'm sure some accountants would be furious with me for not taking the dual entries with sufficient gravitas.

ME

balances ${pe} ${pe_abc} ${abc} pefund 

Step '(7) ABC Inc. ‘transfers or pays’ 500k from 123456789 to PE_ABC' <<ME

This should be straightforward but amusingly enough up until this point in the simulation no one has actually had any money at all.
But this is our sandbox not a real blockchain, we can do what we want. 
So let's give ABC a windfall and they can turn around and pay PE_ABC.

ME

cleos push action eosio.token create "[\"${abc}\", \"132456798000 EOSDT\"]" -p eosio.token@active 2>/dev/null || true
issuer=`eos get currency stats eosio.token EOSDT | jq -r '.EOSDT.issuer'`
eos push action eosio.token issue "[\"${issuer}\", \"1234567890 EOSDT\", \"memo\"]" -p "${issuer}@active"
eos transfer "${issuer}" "${abc}" "123456789 EOSDT" "Once the issuer always the issuer, sort of." -p "${issuer}@active" 2>/dev/null

balances ${pe} ${pe_abc} ${abc} pefund 

eos transfer "${abc}" "${pe_abc}" '500000 EOSDT' 'Memo: to cover the 10% capital call.'

balances ${pe} ${pe_abc} ${abc} pefund 

Step '(8) PE_ABC ‘transfers or pays’ 500k to PE987654321' << ME

The smart contract triggers some code that executes whenever anyone sends it money. So this should just work.

ME

balances ${pe} ${pe_abc} ${abc} pefund 

eos transfer "${pe_abc}" pefund '500000 EOSDT' "${pe}" -p "${pe_abc}@active"

balances ${pe} ${pe_abc} ${abc} pefund 

Step '(9) Create a simple view that displays:
Total commitment = 5mm
Total contribution = 500k
Total distribution = 0
Total unfunded commitment = 4.5mm' <<ME

The simplest of all views would be to simply dump that row of data from the table like I did earlier. 
Let's see what that looks like, then I'll open up a browser to show something that looks more natural to people who don't read JSON all day.

ME

report 
killall chromium >/dev/null 2>/dev/null

Step '(10) A distribution is a reverse transaction flow with money going from PE Corp. all the way to the bank account 123456789' <<ME
    It wasn't spelled out the actual rules for how distributions work so I made up my own.
    
    The fund owning account sends the contract money with a memo of its own account name to trigger a distribution.
    If the memo specified some other fund's name it would be treated as a contribution, BTW.
    Anyhow, I then select out only those accounts that are 'in good standing'. 
    Which means that they've contributed a % of their commitment equal to or greater than the amount that's been called so far.
    If you're delinquent you're not eligible for distrubtion :)
    Then I try to raise everyone to the same distributed
    which is the amount that's been distrubted to them as a % of their commitment.
    The target for that ratio is chosen based on how much was sent for the distribution.
ME

balances ${pe} ${pe_abc} ${abc} pefund 

eos transfer "${pe}" pefund '100000 EOSDT' "${pe}" -p "${pe}@active"

balances ${pe} ${pe_abc} ${abc} pefund 

report
